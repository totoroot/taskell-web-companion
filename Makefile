# File and directory locations
markdownFile=board.md
cssFile=style/board.css
boardHeaderFile=header.md
presentationFrontmatterFile=frontmatter.md
outFolder=out
boardOutFile=board.html
presentationOutFile=presentation.html

# `xdg-open`for Linux and `open` for macOS
openCommand=xdg-open

# reveal.js options
revealjsURL=../reveal.js
revealjsTheme=dracula
revealjsTransition=convex

# pandoc arguments and commands
pandocBoardArgs=-c $(cssFile) -o $(outFolder)/$(boardOutFile)
pandocPresentationArgs=-c $(cssFile) -o $(outFolder)/$(presentationOutFile)
pandocCommand=pandoc -s --template template/template.html
pandocRevealCommand=pandoc -s -t revealjs -V revealjs-url=$(revealjsURL) -V theme=$(revealjsTheme) -V transition=$(revealjsTransition)

copyStyleCommand=cp $(cssFile) $(outFolder)/style/

.makeDirs:
	mkdir -p $(outFolder)/style

board: .makeDirs
	cat $(boardHeaderFile) $(markdownFile) | $(pandocCommand) $(pandocBoardArgs)
	$(copyStyleCommand)

presentation: .makeDirs
	cat $(presentationFrontmatterFile) $(markdownFile) | $(pandocRevealCommand) $(pandocPresentationArgs)

openBoard:
	$(openCommand) $(outFolder)/$(boardOutFile)

openPresentation:
	$(openCommand) $(outFolder)/$(presentationOutFile)

clean:
	rm -rf $(outFolder)
