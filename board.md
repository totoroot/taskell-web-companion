## Open

- Check out taskell
    - [x] Go to [website](https://taskell.app/)
    - [ ] Clone project
    - [ ] Edit config
- Do something else
    + ≡ Test
    - [ ] Test subtask

## In Progress

- Adapt board theme
    @ 2022-04-29 22:22 CEST
    + ≡ Not sure yet

## Completed

- Put project on Codeberg
    + ≡ Project can be found on [Codeberg](https://codeberg.org/totoroot/taskell-web-companion) or [GitHub](https://github.com/totoroot/taskell-web-companion).
