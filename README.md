# taskell-web-kanban

Web companion for converting Kanban boards created with taskell into static pages and HTML slides with pandoc, reveal.js and custom styles using the Dracula colour palette.
